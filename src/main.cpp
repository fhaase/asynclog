#include "asynclog.h"

#include <random>

int main()
{
  asynclog::Config config;
  config.file_prefix = "./log/test_";
  config.file_suffix = ".log";
  config.max_filecount = 99;
  config.max_filesize = 1024;
  config.overwrite_mode = asynclog::OverwriteMode::Disabled;

  asynclog::Init( std::move(config) );

  constexpr auto max_log_level = static_cast<std::underlying_type<asynclog::LogLevel>::type>( asynclog::LogLevel::Debug);

  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_int_distribution<unsigned> delay(0, 2000);
  std::uniform_int_distribution<unsigned> log_level(0, max_log_level);

  int counter = 0;
  while ( true )
  {
    const auto ll = static_cast<asynclog::LogLevel>( log_level(mt) );

    std::thread thread( [ll, counter]
    {
      asynclog::SetThreadName( "Thread#" + std::to_string(counter) );
      asynclog::AsyncLog( ll, "This is a " + asynclog::detail::ToString(ll) );
    } );
    thread.detach();

    ++counter;
    std::this_thread::sleep_for( std::chrono::milliseconds( delay(mt) ));
  }

  asynclog::DeInit();

  return 0;
}