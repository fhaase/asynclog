#pragma once

#include <boost/asio.hpp>

#include <string>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <memory>
#include <thread>

namespace asynclog
{
  enum class LogLevel
  {
    Emergency = 0,
    Alert,
    Critical,
    Error,
    Warning,
    Notice,
    Informational,
    Debug,
  };

  enum class OverwriteMode
  {
    Enabled,
    Disabled
  };

  struct Config
  {
    std::string file_prefix      = "./log/log_";
    std::string file_suffix      = ".log";
    std::uintmax_t max_filesize  = 1024*1024; // 1 MB
    std::uintmax_t max_filecount = 10;
    OverwriteMode overwrite_mode = OverwriteMode::Disabled;
  };

  namespace detail
  {
    template <typename T, typename std::enable_if<std::is_integral<T>::value, int >::type = 0>
    std::string ToFixedWidthString( T integral, unsigned width )
    {
      std::stringstream sstream;
      sstream << std::setw( width ) << std::setfill( '0') << integral;
      return sstream.str();
    }

    std::string ToString( LogLevel level )
    {
      switch ( level )
      {
        case LogLevel::Emergency :
          return "Emergency";
        case LogLevel::Alert :
          return "Alert";
        case LogLevel::Critical :
          return "Critical";
        case LogLevel::Error :
          return "Error";
        case LogLevel::Warning :
          return "Warning";
        case LogLevel::Notice :
          return "Notice";
        case LogLevel::Informational :
          return "Informational";
        case LogLevel::Debug :
          return "Debug";
        default:
          return "Unknown";
      }
    }

    std::uintmax_t GetSize( const std::string& file )
    {
      // returns the right size on windows and linux
      std::ifstream f(file, std::ifstream::ate | std::ifstream::binary);
      return static_cast<std::uintmax_t>(f.tellg()); 
    }

    bool Exists( const std::string& file )
    {
      std::ifstream f(file);
      return f.good();
    }

    void Truncate( const std::string& file )
    {
      std::ofstream f;
      f.exceptions( std::ofstream::failbit | std::ofstream::badbit );

      try
      { 
        f.open( file, std::ios::out | std::ios::trunc );
      }
      catch ( std::system_error& error )
      {
        std::cerr << "Error truncating " << file << " : " << error.what() << std::endl;
      }
    }

    void Write( const std::string& file, const std::string& line )
    {
      std::ofstream f;
      f.exceptions( std::ofstream::failbit | std::ofstream::badbit );

      try
      { 
        f.open( file, std::ios::out | std::ios::app );
        std::cout << line << '\n';
        f << line << '\n';
      }
      catch ( std::system_error& error )
      {
        std::cerr << "Error writing to " << file << " : " << error.what() << std::endl;
      }
    }

    class Logger
    {
    public:

      void Init( Config&& config )
      {
        config_ = std::make_unique<Config>( std::move(config) );
        count_width_ = ToFixedWidthString( config_->max_filecount, 0 ).size();
        count_ = 0;
        UpdateCurrentPath(); 

        if ( config_->overwrite_mode == OverwriteMode::Disabled)
        {
          do
          {
            if ( !Exists( current_path_ ) )
            {
              break;
            }

            ++count_;
            UpdateCurrentPath();
          } while ( count_ <= config_->max_filecount );

          if ( count_ == config_->max_filecount && Exists( current_path_ ) )
          {
            count_ = 0;
            UpdateCurrentPath();       
          }        
        }

        Truncate(current_path_);

        io_work_ = std::make_unique<boost::asio::io_service::work>( io_service_ );
        thread_ = std::thread( [this]{ this->io_service_.run(); } );
      }

      void DeInit()
      {
        io_work_.reset();
        thread_.join();

        config_.reset();
      }

      void Log( const std::string& line )
      {
        io_service_.post( [this, line]
        {
          const auto next_size = GetSize( current_path_ ) + line.size();

          if ( next_size > config_->max_filesize )
          { 
            count_ = ( count_ + 1 ) % config_->max_filecount;
            this->UpdateCurrentPath();
            Truncate(current_path_);
          }

          Write( current_path_, line );
        });
      }

      bool IsInitialized() const
      {
        return static_cast<bool>(config_);
      }

    private:

      void UpdateCurrentPath()
      {
        current_path_ = config_->file_prefix 
          + ToFixedWidthString( count_, count_width_ ) 
          + config_->file_suffix;
      }

      std::unique_ptr<const Config> config_;
      unsigned count_;
      unsigned count_width_;
      std::string current_path_;

      boost::asio::io_service io_service_;
      std::unique_ptr<boost::asio::io_service::work> io_work_;
      std::thread thread_;
    };

    Logger& GetLogger()
    {
      static Logger logger;
      return logger;
    }

    std::string GetTimeStamp()
    {
      using namespace std::chrono;

      const auto now = system_clock::now();
      std::time_t t = system_clock::to_time_t(now);
      std::array<char,21> buffer;
      strftime(buffer.data(), 21, "%F %T.", localtime(&t));

      const auto since = now.time_since_epoch();
      const auto msSince = duration_cast<milliseconds>( since ).count() % 1000;

      return std::string(buffer.data()) + ToFixedWidthString( msSince, 3);
    }

    std::string& GetThreadName()
    {
      static thread_local std::string thread_name;
      return thread_name;
    }

    std::string Compose( LogLevel log_level, const std::string message )
    {
      return GetTimeStamp() 
        + " | " + GetThreadName()
        + " | " + ToString(log_level) 
        + " | " + message;
    }

  } // detail

  void Init( Config&& config )
  {
    detail::GetLogger().Init( std::move(config) );
  }

  void DeInit()
  {
    auto& logger = detail::GetLogger();

    if ( logger.IsInitialized() )
    {
      logger.DeInit();
    }
  }

  void AsyncLog( LogLevel log_level, const std::string& message )
  {
    auto& logger = detail::GetLogger();

    if ( logger.IsInitialized() )
    {
      logger.Log( detail::Compose( log_level, message ) );
    }
  }

  void SetThreadName( const std::string& name )
  {
    detail::GetThreadName() = name;
  }
}

